import 'package:flutter/material.dart';
import 'package:presentation/pages/blue_screen.dart';
import 'package:presentation/pages/gray_screen.dart';
import 'package:presentation/pages/green_screen.dart';
import 'package:presentation/pages/home_screen.dart';
import 'package:presentation/pages/purple_screen.dart';
import 'package:presentation/pages/red_screen.dart';
import 'package:presentation/pages/yellow_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PageView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.horizontal,
        children: const [
          HomeScreen(),
          PurpleScreen(),
          GrayScreen(),
          BlueScreen(),
          GreenScreen(),
          RedScreen()
        ],
        controller: _controller,
      ),
    );
  }
}
