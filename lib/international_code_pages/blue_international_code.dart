import 'package:flutter/material.dart';

class BlueInternationWidget extends StatelessWidget {
  final int? i;
  const BlueInternationWidget({Key? key, this.i}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Row(
        children: [
          Container(
            color: Colors.green,
            width: 40.0,
            height: 30.0,
            child: const Center(
              child: Text( "Nope"),
            ),
          ),
          const SizedBox(width: 10,),
          Expanded(child: Text('This is the international Code $i'))
        ],
      ),
    );
  }
}
