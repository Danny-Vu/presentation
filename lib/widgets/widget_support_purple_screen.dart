import 'package:flutter/material.dart';

class RedWhiteWidget extends StatefulWidget {
  const RedWhiteWidget({Key? key}) : super(key: key);

  @override
  State<RedWhiteWidget> createState() => _RedWhiteWidgetState();
}

class _RedWhiteWidgetState extends State<RedWhiteWidget> {
   bool _isChangeColor = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        setState(() {
          _isChangeColor = !_isChangeColor;
        });
      },
      child: Container(
        height: 50,
        width: 100,
        color: (_isChangeColor == true) ? Colors.red: Colors.white,
      ),
    );
  }

}
