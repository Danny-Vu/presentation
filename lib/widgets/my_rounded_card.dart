import 'package:flutter/material.dart';

class MyRoundedCard extends StatelessWidget {
  final Color? color;
  final Widget? child;

  const MyRoundedCard({Key? key, this.color, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: color,
      child: Column(
        children: [
          Container(
            height: 200.0,
          ),
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16.0),
                      topRight: Radius.circular(16.0))),
              child: child,
            ),
          ),
        ],
      ),
    );
  }
}
