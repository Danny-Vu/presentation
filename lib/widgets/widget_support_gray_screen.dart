import 'package:flutter/material.dart';

class Tile extends StatefulWidget {
  final Color? color;
   const Tile({this.color});

  @override
  State<Tile> createState() => _TileState();
}

class _TileState extends State<Tile> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.color,
      width: 100,
      height: 100,
    );
  }
}


/*
class Tile extends StatefulWidget {
  final Color? color;

  const Tile({Key? key, this.color}) : super(key: key);

  @override
  State<Tile> createState() => _TileState();
}

class _TileState extends State<Tile> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.color,
      width: 100,
      height: 100,
    );
  }
}
*/
