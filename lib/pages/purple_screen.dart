import 'package:flutter/material.dart';
import 'package:presentation/widgets/widget_support_purple_screen.dart';

class PurpleScreen extends StatefulWidget {
  const PurpleScreen({Key? key}) : super(key: key);

  @override
  State<PurpleScreen> createState() => _PurpleScreenState();
}

class _PurpleScreenState extends State<PurpleScreen> {
  bool _isChangeColor = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Build()'),
        ),
        body: Container(
          color: Colors.purple,
          child: Column(
            children: [
              Card(
                child: Container(
                  height: 50,
                  color: Colors.yellow,
                ),
              ),
              Card(
                child: Container(
                  height: 50,
                  color: Colors.yellow,
                ),
              ),
              Card(
                child: Container(
                  height: 50,
                  color: Colors.yellow,
                ),
              ),
             //const RedWhiteWidget()
              _widgetFunction()
            ],
          ),
        ),
      ),
    );
  }

  Widget _widgetFunction(){
    return GestureDetector(
      onTap: (){
        setState(() {
        _isChangeColor = !_isChangeColor;
        });
      },
      child: Container(
        height: 50,
        width: 100,
        color: (_isChangeColor == true) ? Colors.red: Colors.white,
      ),
    );
  }
}


