import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Performance & Optimization'),
      ),
      body: const Center(
        child: Text('"HOW TO CODE CHUẨN QUỐC TẾ"', style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold, fontSize: 20.0),),
      ),
    );
  }
}