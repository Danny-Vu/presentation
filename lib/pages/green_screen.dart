import 'package:flutter/material.dart';

import '../widgets/my_rounded_card.dart';

class GreenScreen extends StatelessWidget {
  const GreenScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyRoundedCard(
        color: Colors.green,
        child:
        ListView.builder(
            itemCount: 40,
            itemBuilder: (context, int index) {
              return _myFibonacci(index);
            }
        )
    );
  }
  
  int _fibonacci(int n){
    return (n<=1) ? n : _fibonacci(n-1) + _fibonacci(n-2);
  }
  
  Widget _myFibonacci(int n){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Container(
            height: 30.0,
            color: Colors.green,
          ),
          Text("My fibonacci is ${_fibonacci(n)}")
        ],
      ),
    );
  }
}



