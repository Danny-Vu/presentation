import 'package:flutter/material.dart';
import 'package:presentation/international_code_pages/blue_international_code.dart';
import 'package:presentation/widgets/my_rounded_card.dart';

class BlueScreen extends StatelessWidget {
  const BlueScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MyRoundedCard(
      color: Colors.blue,
      child:
        ListView.builder(
          itemCount: 35,
            itemBuilder: (context, int index) {
              return (index == 30) ? MyExpensiveWidget() : const MyNormalWidget();
            }
        )
    );
  }
}

class MyExpensiveWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        for (int i = 0; i < 100; i++) const MyNormalWidget()
      ],
    );
  }
}

class MyNormalWidget extends StatelessWidget {
  const MyNormalWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Row(
        children: [
          Container(
            color: Colors.green,
            width: 40.0,
            height: 30.0,
            child: const Center(
              child: Text( "Nope"),
            ),
          ),
          const SizedBox(width: 10,),
          const Expanded(child: Text('How to improve performance for your app'))
        ],
      ),
    );
  }
}


