import 'package:flutter/material.dart';

import '../widgets/widget_support_gray_screen.dart';

class GrayScreen extends StatefulWidget {
  const GrayScreen({Key? key}) : super(key: key);

  @override
  State<GrayScreen> createState() => _GrayScreenState();
}

class _GrayScreenState extends State<GrayScreen> {
    final List<Widget> _listTile = [ Tile( color: Colors.red,), Tile(color: Colors.blue,)];
 // final List<Widget> _listTile = [ Tile(key: UniqueKey(), color: Colors.red,), Tile(key: UniqueKey(), color: Colors.blue,)];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: _swapTwoTileWidget,
            child: const Icon(Icons.swap_horiz),
          ),
          appBar: AppBar(
            title: const Text('Key'),
          ),
            body: Container(
              color: Colors.grey,
              child: Center(
                child: Row(
                  children: _listTile,
                ),
              ),
            )),
    );
  }

  void _swapTwoTileWidget() {
    setState(() {
      _listTile.insert(1, _listTile.removeAt(0));
    });
  }
}



